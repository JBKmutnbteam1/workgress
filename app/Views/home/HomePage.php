<?php
include('config_google.php');
include('config_facebook.php');
//session_destroy();

//google_btn
$login_button = $google_client->createAuthUrl();

//facebook_btn
$facebook_helper = $facebook->getRedirectLoginHelper();

$facebook_permissions = ['email']; // Optional permissions
$facebook_login_url = $facebook_helper->getLoginUrl('https://wg-test.herokuapp.com/UserFacebookController/Facebook_Login', $facebook_permissions);
//$facebook_login_url = '<a href="'.$facebook_login_url.'"><img src="assets\img\btn_facebook.png" width="300" height="60"/></a>';

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Workgress</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist2/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="dist2/css/landing-page1.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="plugins/toastr/toastr.min.js"></script>

  <!-- Animate.css -->
  <link rel="stylesheet" href="assets/course/css/animate.css">

  <!-- Theme style  -->
  <link rel="stylesheet" href="assets/course/css/style.css">

  <!-- Modernizr JS -->
  <script src="assets/course/js/modernizr-2.6.2.min.js"></script>

</head>

<?php
if (session('correct')) : ?>
  <script type="text/javascript">
    $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 5000
      });
      Toast.fire({
        icon: 'success',
        title: '<?php echo session('correct') ?>' + '<?php $this->session = \Config\Services::session();
                                                      $this->session->start();
                                                      $this->session->remove($this->session->get("Email"));
                                                      $this->session->destroy(); ?>'
      })
    });
  </script>
<?php
elseif (session('incorrect')) : ?>
  <script type="text/javascript">
    $(function() {
      $(document).Toasts('create', {
        class: 'bg-danger',
        title: 'Workgress',
        subtitle: '&nbsp&nbsp&nbsp&nbsp&nbspกรุณาลองใหม่&nbsp&nbsp&nbsp&nbsp',
        body: '<?php echo session('incorrect') ?>' + '<?php $this->session = \Config\Services::session();
                                                      $this->session->start();
                                                      $this->session->remove($this->session->get("Email"));
                                                      $this->session->destroy(); ?>'
      })
    });
  </script>
<?php
elseif (session('warning')) : ?>
  <script type="text/javascript">
    $(function() {
      $(document).Toasts('create', {
        class: 'bg-warning',
        title: 'Workgress',
        subtitle: '&nbsp&nbsp&nbsp&nbspกรุณาลองใหม่&nbsp&nbsp&nbsp&nbsp',
        body: '<?php echo session('warning') ?>' + '<?php $this->session = \Config\Services::session();
                                                    $this->session->start();
                                                    $this->session->remove($this->session->get("Email"));
                                                    $this->session->destroy(); ?>'
      })
    });
  </script>
<?php
endif
?>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <ul class="navbar-nav ml-5">
        <a href="<?php echo base_url('/home'); ?>" class="navbar-brand">
          <img src="<?php echo base_url('assets/img/logo.png'); ?>" width="50" height="35" class="rounded-circle">
        </a>
      </ul>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('/home'); ?>">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">หมวดหมู่ <i class="fas fa-th-large"></i></a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <!-- <li><a href="#" class="dropdown-item">Some action </a></li>
                <li><a href="#" class="dropdown-item">Some other action</a></li> -->

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">IT</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">PostgreSql</a>
                  </li>

                  <!-- Level three dropdown-->
                  <li class="dropdown-submenu">
                    <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">PHP</a>
                    <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">Codeigniter 4</a></li>
                      <li><a href="#" class="dropdown-item">Laravel</a></li>
                    </ul>
                  </li>
                  <!-- End Level three -->

                  <li><a href="#" class="dropdown-item">Selenium</a></li>
                  <li><a href="#" class="dropdown-item">AdoDB</a></li>
                </ul>
              </li>
              <!-- End Level two -->
            </ul>
          </li>

        </ul>

        <!-- SEARCH FORM -->
        <div class="container">
          <ul class="nav navbar-nav mx-auto">

            <form class="form-inline ml-1 ml-md-1">
              <div class="input-group">
                <div class="inputlong">
                  <input type="text" class="form-control" placeholder="ค้นหาคอร์สเรียนได้ที่นี่">
                </div>
                <div class="input-group-append">
                  <button class="btn btn-secondary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </ul>
        </div>
        <!-- SEARCH FORM -->

        <!-- Right navbar links -->

        <div class="navbar-collapse collapse w-200 order-3 dual-collapse">
          <ul class="order-1 order-md-5 navbar-nav navbar-no-expand ml-auto">
            <!-- Messages Dropdown Menu -->
            <div class="input-group input-group-sm">
              <!-- Notifications Dropdown Menu -->
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                <b>เข้าสู่ระบบ</b>
              </button>
              <div class="magin-ll">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-register">
                  <b>ลงทะเบียน</b>
                </button>
              </div>
            </div>
          </ul>
        </div>
      </div>
    </nav>
    <!-- /.navbar -->

    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <header class="masthead text-white text-center">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">
              <h1 class="mb-5">ยินดีต้อนรับเข้าสู่ Workgress</h1>
              <h3>คุณพร้อมที่จะเรียนรู้สิ่งใหม่หรือยัง
                ถ้าพร้อมแล้ว กดปุ่มด้านล่างนี้เลย
                เพราะการเรียนรู้ไม่มีที่สิ้นสุด</h3>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
              <form>
                <div class="form-row">

                  <div class="col-12 ">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">
                      <b>เข้าสู่ระบบ</b>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </header>


      <!-- /.content-header -->

      <!-- Main content -->

      <div class="colorlib-loader"></div>
      <div class="colorlib-classes">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-box">
              <h2>หลักสูตร ที่มีใน Workgress</h2>
              <p>______________________________________________________________________________________________________________________________</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-1.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Developing Mobile Apps</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-2.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Convert PSD to HTML</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-3.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Convert HTML to WordPress</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-4.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Developing Mobile Apps</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-5.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Learned Smoke Effects</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
            <div class="col-md-4 animate-box">
              <div class="classes">
                <div class="classes-img" style="background-image: url(assets/course/images/classes-6.jpg);">
                  <span class="price text-center"><small>$450</small></span>
                </div>
                <div class="desc">
                  <h3><a href="#">Convert HTML to WordPress</a></h3>
                  <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                  <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div id="colorlib-subscribe" class="subs-img" style="background-image: url(assets/course/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
          <div class="overlay"></div>
          <div class="container">


          </div>
        </div>
      </div>

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Profile</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2020 <a href="$">Workgress</a>.</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1
      </div>
    </footer>

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">เข้าสู่บัญชี Workgress ของคุณ</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <div class="card-body login-card-body">
                <p class="login-box-msg">เข้าสู่ระบบเพื่อเริ่มระบบของคุณ</p>

                <form action="<?= site_url('/UserController/User_Login') ?>" method="post" role="form" id="quickForm">

                  <div class="input-group mb-3">
                    <input type="email" name="Email_Login" id="Email_Login" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                      </div>
                    </div>
                  </div>
                  <span id="email_check_login"></span>
                  <div class="input-group mb-3">

                    <input type="password" name="Password_Login" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-8">
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                      <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>

                <div class="social-auth-links text-center mb-3">
                  <p>- หรือ -</p>
                  <a href="<?php echo $facebook_login_url ?>" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i> Login With Facebook
                  </a>
                  <a href="<?php echo $login_button ?>" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i> Login With Google+
                  </a>
                </div>
                <!-- /.social-auth-links -->

                <p class="mb-1">
                  <a href="<?php echo base_url('/reset_password'); ?>">ลืมรหัสผ่านใช่หรือไม่ ?</a>
                </p>
                <p class="mb-0">
                  <a href="" data-toggle="modal" data-target="#modal-register">สมัครสมาชิกใหม่</a>
                  </button>
                </p>
              </div>
              <!-- /.login-card-body -->
            </div>

          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Content Wrapper. Contains page content -->
    <!-- ./wrapper -->
    <div class="modal fade" id="modal-register">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">สมัครสมาชิก Workgress ของคุณ</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <div class="card-body login-card-body">
                <p class="login-box-msg">สมัครสมาชิกใหม่เพื่อเริ่มเรียนรู้</p>

                <form role="form" action="<?= site_url('/UserController/User_Register') ?>" method="post" id="RegisterForm">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="Full_Name_Register">ชื่อ-นามสกุล :</label>
                      <div class="input-group mb-3">
                        <input type="text" name="Full_Name_Register" class="form-control" id="Full_Name_Register" placeholder="ใส่ ชื่อ-นามสกุล">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-user"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">อีเมล :</label>
                      <div class="input-group mb-3">
                        <input type="email" name="Email_Register" class="form-control" id="Email_Register" placeholder="Enter email">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span id="email_result"></span>
                    <div class="form-group">
                      <label for="password">รหัสผ่าน :</label>
                      <div class="input-group mb-3">
                        <input type="password" name="Password_Register" class="form-control" id="password" placeholder="Password">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-eye-slash" id="eye"></span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputPassword2">ยืนยันรหัสผ่าน :</label>
                      <div class="input-group mb-3">
                        <input type="password" name="password_confirm" class="form-control" id="exampleInputPassword2" placeholder="Password">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-eye-slash" id="eye1"></span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                        <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary">ยืนยัน</button>
                </form>
                <!-- /.social-auth-links -->
              </div>
              <!-- /.login-card-body -->
            </div>

          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-forgot">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">ลืมรหัสผ่าน</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card">
              <div class="card-body login-card-body">
                <p class="login-box-msg">ลืมรหัสผ่าน Workgress</p>

                <form role="form" action="<?= site_url('/UserController/User_Forget_Password') ?>" method="post" id="ForgetForm">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">อีเมล :</label>
                      <input type="email" name="Email_Forget" class="form-control" id="Email_Forget" placeholder="กรอกอีเมลที่ต้องการจะเปลี่ยนรหัสผ่าน">
                    </div>
                    <span id="email_check_forget"></span>
                    <!-- <div class="form-group">
                      <label for="password">รหัสผ่านใหม่ :</label>
                      <input type="password" name="Password_Forget" class="form-control" id="Password_Forget" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword2">ยืนยันรหัสผ่าน :</label>
                      <input type="password" name="Password_Forget_Confirm" class="form-control" id="Password_Forget_Confirm" placeholder="Password">
                    </div> -->

                  </div>
                  <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary">ยืนยัน</button>
                </form>
                <!-- /.social-auth-links -->
              </div>
              <!-- /.login-card-body -->
            </div>

          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <!-- Content Wrapper. Contains page content -->
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist2/js/adminlte.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- jquery-validation -->
    <script src="plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="plugins/jquery-validation/additional-methods.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist2/js/demo.js"></script>
</body>

</html>

<!-- Check ของ Login -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#Email_Login').change(function() {
      var Email_Login = $('#Email_Login').val();
      //var Email_Login = document.getElementById("Email_Login").value; 
      console.log(Email_Login);
      if (Email_Login != '') {
        $.ajax({
          url: "<?= site_url('/UserController/Check_Email_Login') ?>",
          method: "POST",
          data: {
            Email_Login: Email_Login
          },
          success: function(data) {
            $('#email_check_login').html(data);
          }
        });
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#Email_Register').change(function() {
      var Email = $('#Email_Register').val();
      //var Email = document.getElementById("Email_Register").value; 
      console.log(Email);
      if (Email != '') {
        $.ajax({
          url: "<?= site_url('/UserController/Check_Email') ?>",
          method: "POST",
          data: {
            Email: Email
          },
          success: function(data) {
            $('#email_result').html(data);
          }
        });
      }
    });
  });
  $(function() {

    $('#eye').click(function() {

      if ($(this).hasClass('fa-eye-slash')) {

        $(this).removeClass('fa-eye-slash');

        $(this).addClass('fa-eye');

        $('#password').attr('type', 'text');

      } else {

        $(this).removeClass('fa-eye');

        $(this).addClass('fa-eye-slash');

        $('#password').attr('type', 'password');
      }
    });
    $('#eye1').click(function() {

      if ($(this).hasClass('fa-eye-slash')) {

        $(this).removeClass('fa-eye-slash');

        $(this).addClass('fa-eye');

        $('#exampleInputPassword2').attr('type', 'text');

      } else {

        $(this).removeClass('fa-eye');

        $(this).addClass('fa-eye-slash');

        $('#exampleInputPassword2').attr('type', 'password');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#Email_Forget').change(function() {
      var Email_Forget = $('#Email_Forget').val();
      //var Email_Login = document.getElementById("Email_Login").value; 
      console.log(Email_Forget);
      if (Email_Forget != '') {
        $.ajax({
          url: "<?= site_url('/UserController/Check_Email_Login') ?>",
          method: "POST",
          data: {
            Email_Login: Email_Forget
          },
          success: function(data) {
            $('#email_check_forget').html(data);
          }
        });
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#quickForm').validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 5
        },
        terms: {
          required: true
        },
      },
      messages: {
        email: {
          required: "Please enter a email address",
          email: "Please enter a vaild email address"
        },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        terms: "Please accept our terms"
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {

    var value = $("#password").val();
    $.validator.addMethod("checklower", function(value) {
      return /[a-z]/.test(value);
    });
    $.validator.addMethod("checkupper", function(value) {
      return /[A-Z]/.test(value);
    });
    $.validator.addMethod("checkdigit", function(value) {
      return /[0-9]/.test(value);
    });

    $('#RegisterForm').validate({
      rules: {
        Full_Name_Register: {
          required: true,
          minlength: 8,
        },
        Email_Register: {
          required: true,
          email: true,
        },
        Password_Register: {
          minlength: 5,
          required: true,
          checklower: true,
          checkupper: true,
          checkdigit: true
        },
        terms: {
          required: true
        },
        password_confirm: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
      },
      messages: {
        Full_Name_Register: {
          required: "กรุณากรองชื่อ-นามสกุลด้วย",
          minlength: "ต้องมากกว่า 8 ตัวขึ้นไป",
        },
        Email_Register: {
          required: "กรุณากรองอีเมลด้วย",
          email: "ช่วยใส่ที่เป็น email เช่น xxxx@gmail.com"
        },
        Password_Register: {
          minlength: "ต้องมากกว่า 5 ตัวขึ้นไป",
          checklower: "ต้องมี ตัวอักษรขนาดเล็กอย่างน้อย 1 ตัว",
          checkupper: "ต้องมี ตัวอักษรขนาดใหญ่อย่างน้อย 1 ตัว",
          checkdigit: "ต้องมี ตัวเลขอย่างน้อย 1 ตัว"
        },
        terms: "Please accept our terms",
        password_confirm: "รหัสผ่านไม่เหมือนกัน กรุณากรองใหม่อีกครั้ง"
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });

    $('#ForgetForm').validate({
      rules: {
        Email_Forget: {
          required: true,
          email: true,
        },
        Password_Forget: {
          minlength: 5,
          required: true,
          checklower: true,
          checkupper: true,
          checkdigit: true
        },
        terms: {
          required: true
        },
        Password_Forget_Confirm: {
          required: true,
          minlength: 5,
          equalTo: "#Password_Forget"
        },
      },
      messages: {
        Email_Register: {
          required: "กรุณากรองอีเมลด้วย",
          email: "ช่วยใส่ที่เป็น email เช่น xxxx@gmail.com"
        },
        Password_Forget: {
          minlength: "ต้องมากกว่า 5 ตัวขึ้นไป",
          checklower: "ต้องมี ตัวอักษรขนาดเล็กอย่างน้อย 1 ตัว",
          checkupper: "ต้องมี ตัวอักษรขนาดใหญ่อย่างน้อย 1 ตัว",
          checkdigit: "ต้องมี ตัวเลขอย่างน้อย 1 ตัว"
        },
        terms: "Please accept our terms",
        Password_Forget_Confirm: "รหัสผ่านไม่เหมือนกัน กรุณากรองใหม่อีกครั้ง"
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<?php include 'Register_Validate_Script.php'; ?>

<!-- Waypoints -->
<script src="assets/course/js/jquery.waypoints.min.js"></script>

<!-- Flexslider -->
<script src="assets/course/js/jquery.flexslider-min.js"></script>

<!-- Main -->
<script src="assets/course/js/main.js"></script>

</body>

</html>